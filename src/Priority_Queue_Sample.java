

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Priority_Queue_Sample {

	public static void main(String[] args) {
		
		Queue<String> queue = new LinkedList<String>();
		queue.add("N");
		queue.add("P");
		queue.add("Z");
		queue.add("B");
		System.out.println(queue);
		queue.remove("Z");
		System.out.println(queue);
		
		System.out.println("==>>");
		
		PriorityQueue<Integer> priorityQueue = new PriorityQueue<Integer>();
		priorityQueue.add(10);
		priorityQueue.add(2);
		priorityQueue.add(5);
		priorityQueue.add(11);
		while (!priorityQueue.isEmpty())
           System.out.println(priorityQueue.poll());
		
		System.out.println("==>>");
		
		PriorityQueue<Book> bookQueue = new PriorityQueue<Book>(new SortBookByBookId());
//		PriorityQueue<Book> bookQueue = new PriorityQueue<Book>(new SortBookByBookName());
		Book book1 = new Book(10 ,"Nitin");
		Book book2 = new Book(1 ,"Golu");
		Book book3 = new Book(14 ,"Raja");
		Book book4 = new Book(5 ,"Vipin");
		Book book5 = new Book(7 ,"Suresh");
		bookQueue.add(book1);
		bookQueue.add(book2);
		bookQueue.add(book3);
		bookQueue.add(book4);
		bookQueue.add(book5);
		while (!bookQueue.isEmpty())
	            System.out.println(bookQueue.poll().book_id);
				System.out.println(bookQueue.poll().book_name);
	}
}


class SortBookByBookId implements Comparator<Book>{

	@Override
	public int compare(Book book1 , Book book2) {
		// TODO Auto-generated method stub
		/*if(book1.book_id > book2.book_id)
				return -1;
		else if(book1.book_id < book2.book_id)
				return 1;*/
//		return book1.book_id - book2.book_id;
		return Integer.compare(book1.book_id , book2.book_id);
	}
}

class SortBookByBookName implements Comparator<Book>{

	@Override
	public int compare(Book book1 , Book book2) {
		// TODO Auto-generated method stub
	return book1.book_name.compareTo(book2.book_name);
	}
}

class Book{
		int book_id;
		String book_name;
		
		public Book() {
			// TODO Auto-generated constructor stub
		}
		
		public Book(int book_id, String book_name) {
			this.book_id = book_id;
			this.book_name = book_name;
		}
}
f;dnihtejhi
cldsmhtrmj
.gerpk9-r6tkt
,o[rejy09]